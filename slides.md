# Go Introduction
<!-- effect=stars -->

---

-

---

## Agenda

1. What is Go? vs NodeJS (info)

2. Go Basics (interactive)

3. What Framework to use? (info + interactive)

4. Running, Compiling (interactive)

5. Testing, Coverage (info + interactive)

6. Resources for setup & self learning (info)

7. Q&A

---

## What Is Go?

- Go is statically typed language, it is developed by Google, and made public in late 2009
- Go uses all the cores on your cpu and can use multi thread managed by the go routine
- Go has ultrafast compilation time
- Go compiles down to single binary and is small in size

## NodeJS

- Dynamically typed, can be statically typed with typescript
- Single threaded and uses only one cpu core
- Slow compilation time
- Requires all the source/compiled files and node_modules for it to run


---

## Go Basics

Go has the followings that are not available in NodeJS:

- Structs
- Pointers
- Goroutines (concurrent processing)
- Channels (communication for concurrent processes)
- Receiver functions
- Multiple return values

Go has the followings that are avaiable in NodeJS but behaves differently

- Interfaces
- Packages
- Modules

Go however does not have **Classes**, **Generics** (yet) & **function/method overloading**


---

## What Framework To Use

1. [GoBuffalo](https://gobuffalo.io/en/)
2. [Echo](https://echo.labstack.com/)
3. [net/http + router library:](https://pkg.go.dev/net/http)
	1. [httprouter](https://github.com/julienschmidt/httprouter)
	2. [gorilla/mux](https://github.com/gorilla/mux)

---

## Running, Compiling

Run a single file
```bash
go run main.go
```

Run multiple files
```bash
go run main.go server.go
```

Build
```bash
go build -o application_name
```

Cross Compilation
```bash
GOOS=darwin GOARCH=amd64 go build -o application_name
```

---

## Testing, Coverage

1. Run all test
2. Run single test
3. Coverage
4. Coverage %

---

## Resources for Setup & Self Learning

### Go installation
https://golang.org/doc/install

### Golden standards everybody can agree on
https://golang.org/doc/effective_go

### Go tutorial
https://tour.golang.org/welcome/1

### Development environment setup
https://code.visualstudio.com/docs/languages/go

### Goroutines & channels
https://blog.golang.org/codelab-share

---

## Q&A

## Project structure?

>It depends on the project size, small project can have everything in main package with different files
>Large projects: mvc, clean coding, DDD or anything that works

## Why gRPC
>Because the swagger tooling for Go is not as good as NestJS, there are mainly two types of workflow:
>a) using code comments as swagger annotation and generate from such comments
>b) programmatic annotation just like NestJS, still feels like a chore (the application doesn't need it to run)
>
>Leveraging on protobuf, the code is self documententing, no extra work for documentation

## Why Go?
>There are multiple reasons to this:
>a) the existence of very good tooling is the area of gRPC
>b) benefit from using gRPC service vs RESTful service

---

# The End
<!-- effect=fireworks -->
