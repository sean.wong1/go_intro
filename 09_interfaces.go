package main

import "fmt"

type Joker interface {
	intro()
	tellJokes()
}

type Noob struct {
	name string
}

func (n Noob) intro() {
	fmt.Println("Hi my name is " + n.name + ", I am a noob joker")
}

func (n Noob) tellJokes() {
	fmt.Println("and eh hehe eh ahah")
}

type Pro struct {
	name string
}

func (p Pro) intro() {
	fmt.Println("Hi my name is " + p.name + " and I am a pro joker")
}

func (p Pro) tellJokes() {
	fmt.Println("AND VSCODE THE BEST CODE EDITOR")
}

type StandUpComedyShow struct{}

func (s StandUpComedyShow) start(joker Joker) {
	joker.intro()
	joker.tellJokes()
}

func main() {
	noob := Noob{name: "xDD"}
	pro := Pro{name: "kekw"}

	ss := StandUpComedyShow{}
	ss.start(noob)
	fmt.Println("")
	ss.start(pro)
}
