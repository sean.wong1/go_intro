package main

import (
	"fmt"
	"go_intro/util"
)

func main() {
	cfg := util.Config{ExportedField: "public", UnexportedField: "private"}
	fmt.Println(cfg.ExportedField)
	fmt.Println(cfg.UnexportedField)
}
