package main

import "fmt"

const oneInt int = 1

const (
	constString = "string"
)

func main() {
	fmt.Println(oneInt)
	fmt.Println(constString)
}
