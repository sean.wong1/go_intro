package main

import "fmt"

func main() {
	for i := 50; i < 55; i++ {
		fmt.Println(i)
	}

	someArr := []string{"a", "b", "c", "d"}
	for i, v := range someArr {
		fmt.Printf("i: %d, v: %s\n", i, v)
	}

	counter := 0
	for {
		if counter <= 5 {
			counter++
			fmt.Println(counter)
			continue
		}
		break
	}

namedLoop:
	for {
		for {
			if counter >= 10 {
				break namedLoop
			}
			counter++
			fmt.Println(counter)
		}
	}
}
