package main

import "fmt"

var someString string = "some-string"
var anotherString = "another-string"

var (
	someNumber    int = 1
	anotherNumber     = 2
)

func main() {
	someBool := true
	var anotherBool bool

	someBoolArr := []bool{true, false}

	fmt.Println(someString)
	fmt.Println(anotherString)
	fmt.Println(someNumber)
	fmt.Println(anotherNumber)
	fmt.Println(someBool)
	fmt.Println(anotherBool)
	fmt.Printf("%v\n", someBoolArr)
}
