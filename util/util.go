package util

type Config struct {
	ExportedField   string
	UnexportedField string
}

func ExportedFunc() string {
	return "I can be called from other packages"
}

func unexportedFunc() string {
	return "I cannot be called from other packages"
}
