package main

import "fmt"

type Person struct {
	Name string
}

func main() {
	p := Person{Name: "John Doe"}
	fmt.Println("orignal name is: " + p.Name)

	nonPtrArg(p)
	fmt.Println("name after non pointer argument: " + p.Name)

	ptrArg(&p)
	fmt.Println("name after pointer argument: " + p.Name)

}

func nonPtrArg(person Person) {
	person.Name = "New Name given by non pointer argument"
}

func ptrArg(person *Person) {
	person.Name = "New Name given by pointer argument"
}
