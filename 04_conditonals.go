package main

import "fmt"

func main() {
	if true {
		fmt.Println("TRUE")
	}

	if false {
		fmt.Println("FALSE")
	} else if true {
		fmt.Println("OR TRUE")
	} else {
		fmt.Println("OTHERWISE THIS MESSAGE")
	}
}
