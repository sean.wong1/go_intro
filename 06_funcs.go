package main

import "fmt"

func main() {
	noArg()
	funcWithArg("John Doe", 21)
	funcWithCombinedType("John", "Doe", true)
	variadicArg("James", "John", "Maria")

	fmt.Println(returnSingleVal())

	_, err := returnMultiVal()
	if err != nil {
		fmt.Println(err)
	}

	result := returnNamedVal()
	fmt.Println(result)
}

func noArg() {
	fmt.Println("I dont take any arguments")
}

func funcWithArg(name string, age int) {
	fmt.Printf("my name is %s, I am %d years old\n", name, age)
}

func funcWithCombinedType(name, lastname string, noob bool) {
	fmt.Printf("My name is %s %s, I am a noob: %v\n", name, lastname, noob)
}

func variadicArg(names ...string) {
	fmt.Println("I take variadic arguments\n")
	fmt.Printf("Received %v\n", names)
}

func returnSingleVal() int {
	randomNumber := 2
	return randomNumber
}

func returnMultiVal() (int, error) {
	return 0, fmt.Errorf("Something went wrong")
}

func returnNamedVal() (power int) {
	power = 100
	return
}
