package main

import "fmt"

type SomeStruct struct {
	Field string
}

func main() {
	ss1 := SomeStruct{"some-struct"}
	ss2 := SomeStruct{Field: "another-struct"}

	fmt.Printf("some struct: %+v\n", ss1)
	fmt.Println(ss2.Field)
}
