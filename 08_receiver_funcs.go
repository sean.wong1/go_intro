package main

import "fmt"

type Person struct {
	Name string
}

func (p Person) NonPtrReceiverFunc() {
	p.Name = "named by non pointer receiver func"
}

func (p *Person) PtrReceiverFunc() {
	p.Name = "named by pointer receiver func"
}

func (p Person) GiveName() string {
	return p.Name
}

func main() {
	p1 := Person{Name: "John Cena"}
	p1.PtrReceiverFunc()
	fmt.Println("Person name: " + p1.GiveName())

	p2 := Person{Name: "The Rock"}
	p2.NonPtrReceiverFunc()
	fmt.Println("Person name: " + p2.GiveName())
}
